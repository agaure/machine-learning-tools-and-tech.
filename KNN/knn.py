import os
from struct import * 
from array import * 
import numpy as np
from sklearn import neighbors
import time
from sklearn.datasets import load_iris
 
def getData(task):
   if task == "Training":
      confirmSize = 60000
   else:
      confirmSize = 10000
   magicNoImg = 2051
   magicNoLabl = 2049
   imageMats, scanImg = [], 0
   labels, scanLab =[], 0
   for root, dirs, files in os.walk("."):
      if not(dirs):
         for oneFile in files:
             fPath = os.path.join(root, oneFile) 
             fhand = open(fPath, 'rb')
             info = fhand.read(8)
             try:
                (magicNo, noOfItems) = unpack(">II",info)
             except:
                print("Not a Data File :",oneFile)
             else:
                if noOfItems == confirmSize: 
                    if magicNo == magicNoImg:
                       dimOfMatrix = fhand.read(8)
                       (noOfRows, noOfCols) =  unpack(">II",dimOfMatrix)
                       images = fhand.read()
                       images=array("B", images)
                       for k in range(0, noOfItems):
                          tempMat=np.zeros((28*28), dtype=int)
                          for i in range(0, noOfRows*noOfCols -1):
                             tempMat[i] = images[(k*noOfRows*noOfCols)+i]
                          imageMats.append(tempMat)
                       scanImg +=1
                                       
                    elif magicNo == magicNoLabl:
                       labelsc = fhand.read()
                       labels = array("b", labelsc)
                       scanLab +=1
                       if scanImg and scanLab:
                          return imageMats, labels
                                   


startTime = time.time()
print("Scanning Data Files..")
(imageMats, labels) = getData("Training")
(testImg, testLabl) = getData("Testing")
print("Running k-NN..")
for k in range(2,3):
   metrics = ["minkowski","euclidean","manhattan","chebyshev","braycurtis","canberra","hamming"]#
   for kNNmetric in metrics:
      runTime = time.time()
      kNNClassifier = neighbors.KNeighborsClassifier(n_neighbors=k,weights='uniform', algorithm='auto', leaf_size=30, p=2, metric=kNNmetric, metric_params=None, n_jobs=-1)
      kNNClassifier.fit(imageMats, labels)
      predicted = kNNClassifier.predict(testImg)
      count=0
      for i in range(0, len(iris.target)-1):
         if predicted[i] == testLabl[i]:
            count+=1
      acc=round(count/len(testLabl)*100, 2)
      settings="\nK: "+str(k)+" Metrics: "+kNNmetric
      print(settings)
      print("Accuracy: ",acc)
      print("Algorithm Run Time: ",round((time.time()-runTime)/60, 2),"Mins")
print("Running Time: ",round((time.time()-startTime)/60, 2),"mins")

