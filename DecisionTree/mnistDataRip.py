import os
from struct import * 
from array import * 
import numpy as np
import time


def getData(task):
   if task == "Training":
      confirmSize = 60000
   else:
      confirmSize = 10000
   magicNoImg = 2051
   magicNoLabl = 2049
   imageMats, scanImg = [], 0
   labels, scanLab =[], 0
   for root, dirs, files in os.walk("."):
      if not(dirs):
         for oneFile in files:
             fPath = os.path.join(root, oneFile) 
             fhand = open(fPath, 'rb')
             info = fhand.read(8)
             try:
                (magicNo, noOfItems) = unpack(">II",info)
             except:
                print("Not a Data File :",oneFile)
             else:
                if noOfItems == confirmSize: 
                    if magicNo == magicNoImg:
                       dimOfMatrix = fhand.read(8)
                       (noOfRows, noOfCols) =  unpack(">II",dimOfMatrix)
                       images = fhand.read()
                       images=array("B", images)
                       im = np.asarray(images)
                       im = im.reshape(noOfItems,-1)
                       for k in range(0, noOfItems):
                          tempMat=np.zeros((28*28), dtype=int)
                          for i in range(0, noOfRows*noOfCols -1):
                             tempMat[i] = images[(k*noOfRows*noOfCols)+i]
                          imageMats.append(tempMat)
                       scanImg +=1
                                       
                    elif magicNo == magicNoLabl:
                       labelsc = fhand.read()
                       labels = array("B", labelsc)
                       labels = np.asarray(labels)
                       scanLab +=1
                       if scanImg and scanLab:
                          return im, labels
