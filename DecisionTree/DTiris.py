import os
from struct import * 
from array import * 
import numpy as np
from sklearn import tree
from skimage import feature
from sklearn.ensemble import RandomForestClassifier
import mnistDataRip as md
from matplotlib import pyplot as plt
from sklearn.metrics import classification_report, accuracy_score 
import time
 
                                   
def balenceDataSet(imageMats, labels, n):
   currentItem = 0
   countOcc = [0]*10
   imageMatsNew = []
   labelsNew = []
   for img, lab in zip(imageMats, labels):
      if (countOcc[int(lab)] < (len(imageMats)/n)):
         imageMatsNew.append(img)
         labelsNew.append(lab)
         countOcc[int(lab)] +=1
   return imageMatsNew, labelsNew

def hogFeatures(imageMats):
   hogFeat = []
   for img in imageMats:
      fd = feature.hog(img.reshape((28, 28)), orientations=9, pixels_per_cell=(14, 14), cells_per_block=(1, 1), visualise=False)
      hogFeat.append(fd) 
   hog_features = np.array(hogFeat, 'float64') 
   return hog_features


def trainClassifier(imageMats, labels, Algo):
   if Algo == "DT":
      DecisionClassifier = tree.DecisionTreeClassifier()
   else :
      (_, noOfTrees) = Algo
      DecisionClassifier = RandomForestClassifier(n_estimators = noOfTrees)
   DecisionClassifier.fit(imageMats, labels)
   return DecisionClassifier


def randomForestVersion(Algo):
   Accuracy = []
   TreeNo = []
   Error = []
   runTime = time.time()
   for kTree in range(1, Algo[1]+1):
      Predictor = trainClassifier(imageMats, labels, Algo)
      predicted = Predictor.predict(testImg)
      countCorr=0
      for i in range(0, len(testLabl)-1):
#   print (predicted[i], testLabl[i])
         if predicted[i] == testLabl[i]:
            countCorr+=1
# print(classification_report(testLabl, predicted))
      error = (len(testLabl) - countCorr)/len(testLabl)
      acc1 = round(accuracy_score(testLabl, predicted),3)
      print("number of Trees:",kTree,".\n-----------------")
      print("Accuracy: ",acc1)
      Accuracy.append(acc1)
      Error.append(error)
      TreeNo.append(kTree)
      print("Algorithm Run Time: ",round((time.time()-runTime)/60, 2),"Mins\n____________________________________\n")
#print("Running Time: ",round((time.time()-startTime)/60, 2),"mins")
   merged = zip(Error, TreeNo)
   minimum = min(merged)
   print(minimum)
   (y, x) = minimum
   plt.title("Number of trees vs Error")
   plt.xlabel("Number of Trees in Random Forest")
   plt.ylabel("Error")
   plt.annotate('Minimum error', xy=(x,y), xytext=(x-30,y+0.1), arrowprops=dict(facecolor='black', shrink=0.01),)
   plt.plot(TreeNo, Error, "r-")
   plt.show()
   
def decisionTreeVersion(Algo):
   Predictor = trainClassifier(imageMats, labels, Algo)
   predicted = Predictor.predict(testImg)
  # print(testLabl)
   print(classification_report(testLabl, predicted))
   acc1 = round(accuracy_score(testLabl, predicted),3)
   print("Accuracy: ",acc1)

startTime = time.time()
print("Scanning Data Files..")
(imageMats, labels) = md.getData("Training")
(testImg, testLabl) = md.getData("Testing")

(imageMats, labels) = balenceDataSet(imageMats, labels, 60)
(testImg, testLabl) = balenceDataSet(testImg, testLabl, 100)

imageMats = hogFeatures(imageMats)
testImg = hogFeatures(testImg)


noOfTrees = 50
#noOfTrees = input("\nEnter the number of tree in Forest for Random Forest:  ")
#Algo = ("Random Forest", noOfTrees)
#randomForestVersion(Algo)
try:
   noOfTrees = int(noOfTrees)
   Algo = ("Random Forest", noOfTrees)
   print("\nRunning Random forest decision-Tree Algo upto", noOfTrees,"Trees\n===============================================")
   randomForestVersion(Algo)

except :
   Algo = "DT"
   print("Running Decision-Tree Algo..\n=============================")
   decisionTreeVersion(Algo)
runTime = time.time()


      

