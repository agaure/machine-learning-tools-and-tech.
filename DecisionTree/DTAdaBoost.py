import os
from struct import * 
from array import * 
import numpy as np
from skimage import feature
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
import mnistDataRip as md
import matplotlib.pyplot as plt 
from sklearn.metrics import classification_report, accuracy_score
import time
 

def balenceDataSet(imageMats, labels, n):
   currentItem = 0
   countOcc = [0]*10
   imageMatsNew = []
   labelsNew = []
   for img, lab in zip(imageMats, labels):
      if (countOcc[int(lab)] <= (len(imageMats)/n)):
         imageMatsNew.append(img)
         labelsNew.append(lab)
         countOcc[int(lab)] +=1
   return imageMatsNew, labelsNew


def getValidationData(imageMats, labels):
   currentItem = 10000
   countOcc = [0]*10
   imageMatsNew = []
   labelsNew = []
   for img, lab in zip(imageMats[10000 :], labels[10000 :]):
      if (countOcc[int(lab)] <= 100):
         imageMatsNew.append(img)
         labelsNew.append(lab)
         countOcc[int(lab)] +=1
   return imageMatsNew, labelsNew



def hogFeatures(imageMats):
   hogFeat = []
   for img in imageMats:
      fd = feature.hog(img.reshape((28, 28)), orientations=9, pixels_per_cell=(14, 14), cells_per_block=(1, 1), visualise=False)
      hogFeat.append(fd) 
   hog_features = np.array(hogFeat, 'float64') 
   return hog_features

                                   
def trainClassifier(imageMats, labels, weakDT, Algo):
   if Algo == 'Random Forest':
      DecisionClassifier = RandomForestClassifier(max_depth = weakDT[2], n_estimators = (weakDT[1]))
      DecisionClassifier.fit(imageMats, labels)
   else :
      DecisionClassifier = AdaBoostClassifier(base_estimator = weakDT[0], n_estimators = weakDT[1])
      DecisionClassifier.fit(imageMats, labels)
   return DecisionClassifier


def getSomeWeakDTs():
   TreeDepth = 10
   weakDT = tree.DecisionTreeClassifier(max_depth = TreeDepth)
   weakDTs = []
   for trees in range(1, 200):
      weakDTs.append((weakDT, trees, TreeDepth))
   return weakDTs


def compareAdaRand(Ada, Rand):
   PredictorA = Ada.fit(imageMats, labels)
   PredictorR = Rand.fit(imageMats, labels)
   predictedA = PredictorA.predict(validImg)
   predictedR = PredictorR.predict(validImg)
   accA = round(accuracy_score(validLabl, predictedA),3)
   accR = round(accuracy_score(validLabl, predictedR),3)
   print("Adaboost on Test set: ")
   print(classification_report(validLabl, predictedA))
   print("Accuracy: ", accA)
   print("\nRandom Forest on Test set: ")
   print(classification_report(validLabl, predictedR))
   print("Accuracy", accR)
   
startTime = time.time()
print("Scanning Data Files..")

(imageMats, labels) = md.getData("Training")
(testImg, testLabl) = md.getData("Testing")

(imageMats, labels) = balenceDataSet(imageMats, labels, n = 60)
(testImg, testLabl) = balenceDataSet(testImg, testLabl, n = 100)
(validImg, validLabl) = getValidationData(imageMats, labels)

imageMats = hogFeatures(imageMats)
testImg = hogFeatures(testImg)
validImg = hogFeatures(validImg)

runTime = time.time()
#Algo = input("Enter 'r' for Random forest or Adaboost (default): ")
weakDTs = getSomeWeakDTs()
Error = []
Accuracy = []
TreeNo = []

for Algo in ['Random Forest','Adaboost']:
   print("\nRunning ",Algo,"\n==================") 
   print("Number of Trees         Tree Depth              Accuracy")

   for weakDT in weakDTs:
      Predictor = trainClassifier(imageMats, labels, weakDT, Algo)
      predicted = Predictor.predict(testImg)
      countCorr=0
      for i in range(0, len(testLabl)):
    #   print (predicted[i], testLabl[i])
         if predicted[i] == testLabl[i]:
            countCorr+=1
      error = (len(testLabl) - countCorr)/len(testLabl)
      acc1 = round(accuracy_score(testLabl, predicted),3)
      Accuracy.append(acc1)
      Error.append(error)
      TreeNo.append(weakDT[1])
      print( weakDT[1],"\t\t\t ", weakDT[2], "\t\t\t ",acc1)
     # print("Algorithm Run Time: ",round((time.time()-runTime)/60, 2),"Mins\n____________________________\n")

half = int(len(Error)/2)
merged1 = zip(Error[ : half], TreeNo[ : half])
merged2 = zip(Error[half : ], TreeNo[half : ])
minimum1 = min(merged1)
minimum2 = min(merged2)

(y1, x1), (y2, x2) = minimum1, minimum2
plt.title("Number of trees vs Error")
plt.xlabel("Number of Trees")
plt.ylabel("Error")
#plt.annotate('Minimum error', xy=(x1,y1), xytext=(x1-30,y1+0.1), arrowprops=dict(facecolor='black', shrink=0.01),)
plt.plot(TreeNo[ : half], Error[ : half], "g-", TreeNo[half : ], Error[half : ], "b-")
plt.show()

#Rand =  RandomForestClassifier(max_depth = 3, n_estimators = 121)
#Ada = AdaBoostClassifier(base_estimator = tree.DecisionTreeClassifier(max_depth= 3), n_estimators = 131)
#compareAdaRand(Ada, Rand)
    #print("Running Time: ",round((time.time()-startTime)/60, 2),"mins")
      
