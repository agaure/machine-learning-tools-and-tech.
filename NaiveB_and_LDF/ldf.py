import os
import numpy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from pandas import DataFrame
from sklearn.svm import LinearSVC
#2893
 
fCount = 0

noiseChar = "!@#$%^&*()_+-=?/:;[]{}\~.,"
def getFiles(root):
    for root, dirs, files in os.walk(root):
           for oneFile in files:
                   fPath = os.path.join(root, oneFile)
                   fhandle = open(fPath,'r')
                   cont = fhandle.read()
                   contents=""
                   for ch in cont:
                       if ch in noiseChar:
                           ch = ""
                       contents += ch
                   yield oneFile, contents


def makeFrames():
    framesList = DataFrame({"mail": [], "category": [], "part": []})
    fCount = 0
    i = 0
    print("Scaning Files..")
    for root, dirs, files in os.walk('.'):
        if not(dirs):
            rows = []
            fNames = []
            i += 1
            for fileName, contents in getFiles(root):
                fCount += 1
                if (fileName.find("spm") == 0):
                    rows.append({"mail": contents, "category": 0, "part": i})
                    fNames.append(fileName)
                else:
                    rows.append({"mail": contents, "category": 1, "part": i})
                    fNames.append(fileName)
            frames = DataFrame(rows, index=fNames)
            frames = frames.reindex(numpy.random.permutation(frames.index))
            framesList = framesList.append(frames)    
    print("No. of Files Scaned: ",fCount)
    return (framesList)


def partTestTrain():
    groups = framesList.groupby(["part"])
    for i in range(1, 11):
       try:
           testFrames = groups.get_group(i)
           trainFrames = DataFrame({"mail": [], "category": [], "part": []})
       except:
           print("Key Not Found!")
       
       for j in range(1, 11):
           if not(i == j): 
               trainFrames = trainFrames.append(groups.get_group(j))
       yield (trainFrames, testFrames)


def featureExtraction(mailCont, testData, prob):
    countVect = CountVectorizer()
    XTrainCounts = countVect.fit_transform(mailCont)
    testData = countVect.transform(testData)
    if(prob==2):
        termFreqTrans = TfidfTransformer(use_idf=False).fit(XTrainCounts)
        XTrainCounts = termFreqTrans.transform(XTrainCounts)
        testData = termFreqTrans.transform(testData)
    elif (prob==3):
        tiFreqTrans = TfidfTransformer()
        XTrainCounts = tiFreqTrans.fit_transform(XTrainCounts)
        testData = tiFreqTrans.transform(testData)
    return (XTrainCounts, testData)

def svcTrainer(XTrain, Ys):
    return( LinearSVC().fit(XTrain,Ys))

framesList = makeFrames()
for prob in range(1,4):
    correct, total, preFixAcc = 0, 0, 0
    noOfSpam, predictSpam = 0, 0
    falsePos, falseNeg, truePos, trueNeg = 0,0,0,0
    subProb =['a','b','c']
    
    fold=0
    queNo="Que.2 "+subProb[prob-1]+".  Bag of Words.."
    if prob == 2:
        queNo = queNo+" Term-Frequncy Bag of Words.."
    if prob == 3:
        queNo = queNo+" Inverse Document Frequncy Bag of Words.." 
    print(queNo)
    for frames, testCase in partTestTrain():
        mail = frames["mail"].values
        labelY = frames["category"].values
        testData = testCase["mail"].values
        testLabel = testCase["category"].values 
            
#   Feature Extraction-----------------------------------------------
        (XTrainCounts, XTestCounts) = featureExtraction(mail, testData, prob)
        
#   Training Classifier----------------------------------------------
        spmClassifier = svcTrainer(XTrainCounts, labelY)
   
#   Testing K-fold---------------------------------------------------
        predictions = spmClassifier.predict(XTestCounts)

        preTot, preCorr, fold = total, correct, 1+fold
        preTP, preTN, preFP, preFN = truePos, trueNeg, falsePos, falseNeg
        
        for p, r in zip(predictions, testLabel): 
            if (p == r):
                correct +=1
                total +=1
                if p == 1:
                    trueNeg +=1
                else:
                    truePos +=1
            else:
                total +=1
                if p == 1:
                    falseNeg +=1
                else:
                    falsePos +=1
            noOfSpam += (1-r) 
            predictSpam += (1-p)
        thisTot, thisCorr  = total-preTot, correct-preCorr
        thisTP, thisTN, thisFP, thisFN = truePos-preTP, trueNeg-preTN, falsePos-preFP, falseNeg-preFN 
        thisAcc, thisPres = (thisCorr/thisTot), (thisTP/(thisTP+thisFP))
        preFixAcc += thisAcc
        accPre=str(fold)+". Accuracy: "+str(round(thisAcc*100,2))+"% Presision: "+str(round(thisPres,2))    
        print(accPre)
    print("Total no. of Mail Classified: ",total)
    print("No. of correct Classifications: ",correct)
    print("False Positive: ",falsePos," False Negative: ",falseNeg)
    acc ="Accuracy: "+str(round((preFixAcc*10),2))+"%"
    pres ="Presision: "+str(round(truePos/(truePos + falsePos),2))
    rec = "Recall: "+str(round(truePos/(truePos + falseNeg),2))+"\n"
    print(acc,pres,rec)



