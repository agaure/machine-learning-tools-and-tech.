import os
import numpy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
#from sklearn.naive_bayes import BernoulliNB
#from sklearn.naive_bayes import GaussianNB
from pandas import DataFrame
from nltk.corpus import stopwords
from nltk.stem.lancaster import LancasterStemmer
from nltk.stem import WordNetLemmatizer

#2893
 
fCount = 0
stop = set(stopwords.words("english"))
noiseChar = "!@#$%^&*()_+-=?/:;[]{}\~.,"

def getFiles(root):
    for root, dirs, files in os.walk(root):
           for oneFile in files:
                   fPath = os.path.join(root, oneFile)
                   fhandle = open(fPath,'r')
                   cont = fhandle.read()
                   contents=""
                   for ch in cont:
                       if ch in noiseChar:
                           ch = ""
                       contents += ch
                   yield oneFile, contents


def makeFrames():
    framesList = DataFrame({"mail": [], "category": [], "part": []})
    fCount = 0
    i = 0
    print("Scaning Files..")
    for root, dirs, files in os.walk('.'):
        if not(dirs):
            rows = []
            fNames = []
            i += 1
            for fileName, contents in getFiles(root):
                fCount += 1
                if (fileName.find("spm") == 0):
                    rows.append({"mail": contents, "category": 0, "part": i})
                    fNames.append(fileName)
                else:
                    rows.append({"mail": contents, "category": 1, "part": i})
                    fNames.append(fileName)
            frames = DataFrame(rows, index=fNames)
            frames = frames.reindex(numpy.random.permutation(frames.index))
            framesList = framesList.append(frames)    
    print("No. of Files Scaned: ",fCount)
    return (framesList)


def partTestTrain():
    groups = framesList.groupby(["part"])
    for i in range(1, 11):
       try:
           testFrames = groups.get_group(i)
           trainFrames = DataFrame({"mail": [], "category": [], "part": []})
       except:
           print("Key Not Found!")
       
       for j in range(1, 10):
           if not(i == j): 
               trainFrames = trainFrames.append(groups.get_group(j))
       yield (trainFrames, testFrames)


def featureExtraction(mailCont,testData):
    countVect = CountVectorizer()
    XTrainCounts = countVect.fit_transform(mailCont)
    testData = countVect.transform(testData)
    return (XTrainCounts, testData)


def nbTrainer(XTrain, Ys):
    return(MultinomialNB().fit(XTrain,Ys))
#    return(GaussianNB().fit(XTrain.toarray(),Ys))
#    return(BernoulliNB().fit(XTrain,Ys))

def filterStop(text):
    text = text.lower()
    words = text.split()
    text = ""
    for word in words:
        if word not in stop:
            text += word
            text +=" "
    return text 

def stemmThis(text):
    lemmatizer = WordNetLemmatizer()
#    stemmTool = LancasterStemmer()
    words = text.split()
    text =""
    for word in words:
#      word = stemmTool.stem(word)
      word = lemmatizer.lemmatize(word)	
      text += word
      text += " "
    return text

framesList = makeFrames()
for prob in range(1,4):
    correct, total, preFixAcc = 0, 0, 0
    noOfSpam, predictSpam = 0, 0
    falsePos, falseNeg, truePos, trueNeg = 0,0,0,0
    subProb =['a','b','c']
    
    fold=0
    queNo="Que.1 "+subProb[prob-1]+")"
    if prob > 1:
        queNo = queNo+" Stop Words Removed.."
    if prob == 3:
        queNo = queNo[0 : (len(queNo)-2)]+" and Lammatized.." 
    print(queNo)
    for frames, testCase in partTestTrain():
        mail = frames["mail"].values
        labelY = frames["category"].values
        testData = testCase["mail"].values
        testLabel = testCase["category"].values 
        if (prob > 1):
            n=0
            for oneMail in mail:
                mail[n] = filterStop(oneMail)
                if (prob > 2):
                    mail[n] = stemmThis(mail[n])
                n +=1
            n=0
            for oneMail in testData:
                testData[n] = filterStop(oneMail)
                if (prob > 2):
                    testData[n] = stemmThis(testData[n])
                n +=1
    
#   Feature Extraction-----------------------------------------------
        (XTrainCounts, XTestCounts) = featureExtraction(mail, testData)
        
#   Training Classifier----------------------------------------------
        spmClassifier = nbTrainer(XTrainCounts, labelY)
   
#   Testing K-fold---------------------------------------------------
        predictions = spmClassifier.predict(XTestCounts)

        preTot, preCorr, fold = total, correct, 1+fold
        preTP, preTN, preFP, preFN = truePos, trueNeg, falsePos, falseNeg
        
        for p, r in zip(predictions, testLabel): 
            if (p == r):
                correct +=1
                total +=1
                if p == 1:
                    trueNeg +=1
                else:
                    truePos +=1
            else:
                total +=1
                if p == 1:
                    falseNeg +=1
                else:
                    falsePos +=1
            noOfSpam += (1-r) 
            predictSpam += (1-p)
        thisTot, thisCorr  = total-preTot, correct-preCorr
        thisTP, thisTN, thisFP, thisFN = truePos-preTP, trueNeg-preTN, falsePos-preFP, falseNeg-preFN 
        thisAcc, thisPres = (thisCorr/thisTot), (thisTP/(thisTP+thisFP))
        preFixAcc += thisAcc
        accPre=str(fold)+". Accuracy: "+str(round(thisAcc*100,2))+"% Presision: "+str(round(thisPres,2))    
        print(accPre)
    print("Total no. of Mail Classified: ",total)
    print("No. of correct Classifications: ",correct)
    print("False Positive: ",falsePos," False Negative: ",falseNeg)
    acc ="Accuracy: "+str(round((preFixAcc*10),2))+"%"
    pres ="Presision: "+str(round(truePos/(truePos + falsePos),2))
    rec = "Recall: "+str(round(truePos/(truePos + falseNeg),2))+"\n"
    print(acc,pres,rec)



