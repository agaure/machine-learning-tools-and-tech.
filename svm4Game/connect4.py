import os
import numpy as np
from sklearn.svm import LinearSVC
from sklearn.cross_validation import KFold
from sklearn.metrics import classification_report, accuracy_score
import itertools
import time

StartTime = time.time()
noOfGames = 0
Results = ["win", "loss", "draw"]

def scanFile(fpath):
    print("Scanning Files..")
    for f in os.listdir(fpath):
        if f.endswith(".data") :
            fname = os.path.join(fpath,f)
            fhandle = open(fname, 'r')
            matches = fhandle.readlines()
            for match in matches:
                yield match

def getStats():
    Games = []
    GameRes = []
    index = 0
    for match in scanFile("Data/"):
        oneGameTemp = match.strip('\n').split(',')
        oneGame = [0]*42*3
        for i in range(0,42):
            if oneGameTemp[i] == 'x':
                oneGame[i*3] = 1
            elif oneGameTemp[i] == 'b':
                oneGame[i*3 + 1] = 1
            else: 
                oneGame[i*3 + 2] = 1
        GameRes.append(oneGameTemp[42])
        index +=1
        Games.append(oneGame)
    global noOfGames
    noOfGames = index
    return Games, GameRes

def trainSVM(X, y):
#    print("Trainning Classifier..")
    X_new = LinearSVC().fit(X, y)
    return X_new

def oneVsOneClass(tup, Games, Res):
    (Res1, Res2) = tup
#    print("For Class ", Res1, " and ", Res2, end = " ")
    GamesX, ResX =[],[]
    for game, res in zip(Games, Res):
        if res == Res1 or res == Res2:
            GamesX.append(game)
            ResX.append(res)
    return GamesX, ResX

def oneVsRestClass(ClassRes, Res):
#    print("For Class ", ClassRes, " and Rest", end = " ")
    ResX = []
    for r in Res:
        if r == ClassRes:
            ResX.append(r)
        else:
            ResX.append("rest")
    return ResX

def oneVsOne(Games, Res, TestGames):
    AllPredictions = []
    for twoClass in itertools.combinations(Results, 2):
        GamesX, ResX = oneVsOneClass(twoClass, Games, Res)    
        GamesX = np.asarray(GamesX)
        Predictor = trainSVM(GamesX, ResX)
        TwoClassPred = Predictor.predict(TestGames)
        AllPredictions.append(TwoClassPred)
        global StartTime 
#        print(round((time.time()-StartTime)/60,2), " mins.")
        StartTime = time.time()
    Predictions=[]
    for i in range(0, len(TestGames)):
        if AllPredictions[0][i] == AllPredictions[1][i]:
            Predictions.append(AllPredictions[0][i])       
        elif AllPredictions[1][i] == AllPredictions[2][i]:
            Predictions.append(AllPredictions[1][i])       
        else: 
            Predictions.append(AllPredictions[2][i])
    return Predictions

def oneVsRest(Games, Res, TestGames):
    AllDistance = [] 
    Predictions =[]
    for oneRes in Results:
        ResX = oneVsRestClass(oneRes, Res)
        Predictor = trainSVM(Games, ResX)
        Distance = Predictor.decision_function(TestGames)
        if oneRes != 'win':
            Distance = [d*(-1) for d in Distance]
        AllDistance.append(Distance)
    for i in range(0, len(TestGames)):
        Label = max(AllDistance[0][i], AllDistance[1][i], AllDistance[2][i])
        if AllDistance[0][i] == Label:
            Predictions.append("win")
        elif AllDistance[1][i] == Label:
            Predictions.append("loss")
        else :
            Predictions.append("draw")
    return Predictions
       # p = pred.predict(Games[1:30])
    


print('Initiating Launch...')
Games, Res = getStats()
print('Data Received..')

Tech = input("Enter 0 for  'One-Vs-One' and 1 for 'One-Vs-Rest': ")
if Tech == "1":
    print("\n***One-Vs-Rest***")
else:
    print("\n***One-VS-One***")

kf = KFold(len(Games), n_folds = 5, shuffle = True, random_state = None)
FoldCount = 1
for TrainIndex, TestIndex in kf:
    TrainGames = []
    TrainGameRes = []
    TestGames = []
    TestGameRes = []
    
    for i in TrainIndex:
        TrainGames.append(Games[i])
        TrainGameRes.append(Res[i])
    for i in TestIndex:
        TestGames.append(Games[i])
        TestGameRes.append(Res[i])
            
    TrainGames = np.asarray(TrainGames)
    TestGames = np.asarray(TestGames)

    if Tech == "0" :
        Predictions = oneVsOne(TrainGames, TrainGameRes, TestGames)
    else:
        Predictions = oneVsRest(TrainGames, TrainGameRes, TestGames)
    print(" Fold: ",FoldCount,"\n---------")
    lab = [1,2,3,4]
    print(classification_report(TestGameRes, Predictions))#, labels = lab))
    print("Accuracy: ",round(accuracy_score(TestGameRes, Predictions)*100,3),"%\n_____________________________________________________\n")
    FoldCount+=1

