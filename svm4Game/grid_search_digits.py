"""
============================================================
Parameter estimation using grid search with cross-validation
============================================================


"""

from __future__ import print_function

import os
import numpy as np
from sklearn import datasets
from sklearn.cross_validation import train_test_split
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.svm import SVC

print(__doc__)

# Loading the Digits dataset

#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
def scanFile(fpath):
    print("Scanning Files..")
    for f in os.listdir(fpath):
        if f.endswith(".data") :
            fname = os.path.join(fpath,f)
            fhandle = open(fname, 'r')
            matches = fhandle.readlines()
            for match in matches:
                yield match

def getStats():
    Games = []
    GameRes = []
    index = 0
    for match in scanFile("Data/"):
        oneGameTemp = match.strip('\n').split(',')
        oneGame = [0]*42*3
        for i in range(0,42):
            if oneGameTemp[i] == 'x':
                oneGame[i*3] = 1
            elif oneGameTemp[i] == 'b':
                oneGame[i*3 + 1] = 1
            else: 
                oneGame[i*3 + 2] = 1
        GameRes.append(oneGameTemp[42])
        index +=1
        Games.append(oneGame)
    global noOfGames
    noOfGames = index
    return Games, GameRes


print('Initiating Launch...')
Games, Res = getStats()
print('Data Received..')


#+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



# To apply an classifier on this data, we need to flatten the image, to
# turn the data in a (samples, feature) matrix:
n_samples = len(Games)
X = np.asarray(Games)
#X = digits.images.reshape((n_samples, -1))
y = Res

# Split the dataset in two equal parts
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.2, random_state=0)

# Set the parameters by cross-validation
tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                     'C': [1, 10, 100, 1000]},
                    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]

scores = ['precision', 'recall']

for score in scores:
    print("# Tuning hyper-parameters for %s" % score)
    print()

    clf = GridSearchCV(SVC(C=1), tuned_parameters, cv=5,
                       scoring='%s_weighted' % score)
    clf.fit(X_train, y_train)

    print("Best parameters set found on development set:")
    print()
    print(clf.best_params_)
    print()
    print("Grid scores on development set:")
    print()
    for params, mean_score, scores in clf.grid_scores_:
        print("%0.3f (+/-%0.03f) for %r"
              % (mean_score, scores.std() * 2, params))
    print()

    print("Detailed classification report:")
    print()
    print("The model is trained on the full development set.")
    print("The scores are computed on the full evaluation set.")
    print()
    y_true, y_pred = y_test, clf.predict(X_test)
    print(classification_report(y_true, y_pred))
    print()

# Note the problem is too easy: the hyperparameter plateau is too flat and the
# output model is the same for precision and recall with ties in quality.
